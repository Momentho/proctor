import { AdventofcodeComponent } from './adventofcode/adventofcode.component';
import { BlogPageComponent } from './pages/blog-page/blog-page.component';
import { NotfoundPageComponent } from './pages/notfound-page/notfound-page.component';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes : Routes =[
  // Posts Page.
  { path: '', component: BlogPageComponent },
  // Advent of code.
  { path: 'advent-of-code', component: AdventofcodeComponent },
  // Not Found pages.
  { path: '**', component: NotfoundPageComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class  AppRoutingModule {}

