import { Component, OnInit } from '@angular/core';
import { ThemeStyleService } from './../../themeStyle.service';

@Component({
  selector: '[app-toggle-switch]',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.scss']
})
export class ToggleSwitchComponent implements OnInit {

  constructor( public ThemeStyleService:ThemeStyleService) { }

  activeTheme: string;
  darkTheme = 'dark';
  lightTheme = 'light';
  checkStatus: boolean;

  ngOnInit(): void {
    this.activeTheme = this.ThemeStyleService.currentActive();
    if (this.activeTheme === 'light') {
      this.checkStatus = false;
    } else {
      this.checkStatus = true;
    }
  }


  changeTheme() {
    this.activeTheme = this.ThemeStyleService.currentActive();

    if ( this.activeTheme == 'light' ) {
      this.checkStatus = true;
      this.ThemeStyleService.update(this.darkTheme);

    } else if (this.activeTheme == 'dark') {
      this.checkStatus = false;
      this.ThemeStyleService.update(this.lightTheme);
    }

  }
}
