import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { BaseCardComponent } from './base-card/base-card.component';
import { TextLockComponent } from './text-lock/text-lock.component';


@NgModule({
  declarations: [
    ToggleSwitchComponent,
    BaseCardComponent,
    TextLockComponent,
  ],
  exports: [
    ToggleSwitchComponent,
    BaseCardComponent,
    TextLockComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule{

}
