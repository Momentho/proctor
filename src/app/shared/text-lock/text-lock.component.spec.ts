import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextLockComponent } from './text-lock.component';

describe('TextLockComponent', () => {
  let component: TextLockComponent;
  let fixture: ComponentFixture<TextLockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextLockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextLockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
