import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '.text-lock',
  templateUrl: './text-lock.component.html',
  styleUrls: ['./text-lock.component.scss'],
  encapsulation : ViewEncapsulation.None,
})
export class TextLockComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
