import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '.card',
  templateUrl: './base-card.component.html',
  styleUrls: ['./base-card.component.scss'],
  encapsulation : ViewEncapsulation.None,
})
export class BaseCardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
