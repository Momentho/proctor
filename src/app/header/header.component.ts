import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ThemeStyleService } from './../themeStyle.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation : ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  private authListenerSubs : Subscription;

  // Mobile navigation
  menuStatus: string;

  constructor(
    public ThemeStyleService:ThemeStyleService,
    public router:Router
  ) {}

  public themes = [
      {
          name: 'dark',
          icon: 'brightness_3'
      },
      {
          name: 'light',
          icon: 'wb_sunny'
      }
  ];

  public dynamicUrl: string;

  ngOnInit(): void {


    // Close mobile nav on page change.
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      //window.scrollTo(0, 0)
      this.menuStatus = 'closed';
    });

    // Menu navigation
    this.menuStatus = 'closed';
  }

  ngOnDestroy(): void {
    this.authListenerSubs.unsubscribe();
    this.menuStatus= 'closed';
  }

  setTheme(theme: string) {
      this.ThemeStyleService.update(theme);
  }

  onBurgerClick(): void{
    if(this.menuStatus== 'closed'){
      this.menuStatus= 'open';
    }else {
      this.menuStatus= 'closed';
    }
  }

}
