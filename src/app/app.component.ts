import { Component, OnInit } from '@angular/core';
import { ThemeStyleService } from './themeStyle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  title = 'sofi-mean';

  constructor( private ThemeStyleService:ThemeStyleService){
    this.ThemeStyleService.load();
  }

  ngOnInit(): void {

  }

}
