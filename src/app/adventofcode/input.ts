/*
 * File that contains Codeadvent input.
 */
'use strict';

export const codeAdvent: string = `mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`;

export const codeAdvent1: string = `1721
979
366
299
675
1456`;

