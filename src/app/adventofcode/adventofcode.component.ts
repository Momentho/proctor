import { codeAdvent, codeAdvent1 } from './input';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adventofcode',
  templateUrl: './adventofcode.component.html',
  styleUrls: ['./adventofcode.component.scss']
})
export class AdventofcodeComponent implements OnInit {


  // Advent of Code day 1
  day1Input = [];
  day1solution = 0;


  // Advent of Code day 21.
  codeadventInput:string;
  foods = [];
  myIngredients = [];
  myAllergenes = [];
  allergensToIngredients = {}; // string => Set

  constructor() {}

  ngOnInit(): void {

    /**
     * Advent of Code day 1
     *
     */
     const day1Input = codeAdvent1.split('\n').filter(x => x);
     this.day1Input = day1Input;

     // Loop trough array of items.
     this.day1Input.forEach(item =>{
       const param1 = parseInt(item);

       this.day1Input.forEach(item =>{
        const param2 = parseInt(item);
        const total = param2 + param1;

        //Check if the sum result is 2020.
        if (total === 2020) {
          this.day1solution = param1 * param2;
        }
       })
     })

     //Log solution.
     console.log(this.day1solution);



    /**
     * Advent of Code day 21
     *
     *
     *
     *
     *
     */

    // Parse CodeAdvent input in order to create the food array.
    const lines = codeAdvent.split('\n').filter(x => x);

    lines.forEach(line =>{

      const [ingredients , allergenes] = line.replace(')', '').split(' (contains ').map (x => x.split(/[ ,]+/g));
      this.foods.push({ingredients , allergenes});

    });

    // Push allergenes and ingredients.
    for(var key in this.foods) {

      // Push all the possible ingredients.
      this.foods[key].ingredients.forEach(element => {
        if( !(this.myIngredients.includes(element)) ){
          this.myIngredients.push(element);
        }
      });

      // Push all the possible allergenes.
      this.foods[key].allergenes.forEach(element => {
        if( !(this.myAllergenes.includes(element)) ){
          this.myAllergenes.push(element);
        }
      });
    }

    // Start check all the ingredients in the list.
    for(var key in this.myIngredients) {

      const ingredient = this.myIngredients[key];

      let ingredientsNotContains = [];

      // Check wich foods have the ingredient.
      const listOfFoods = this.foods.filter(food => food.ingredients.includes(ingredient));

      // Take all the allergenes I have to check.
      let allergenesIncluded = [];

      listOfFoods.forEach(food => {
        food.allergenes;
        for (var i in food.allergenes) {
          const allergene = food.allergenes[i];

          if(!(allergenesIncluded.includes(allergene))){
            allergenesIncluded.push(allergene);
          }
        }
      });

      // For each allergene check if the ingredient can bring it.
      allergenesIncluded.forEach(allergene => {
        const totalFoods = this.listAllallergeneFoods(allergene);

        if ( !(totalFoods.length <= listOfFoods.length) ) {
          if(!(ingredientsNotContains.includes(allergene))){
            ingredientsNotContains.push(allergene);
          }
        }
        console.log(ingredientsNotContains);
      })


    }

  }




  // List all foods that have the allergene.
  listAllallergeneFoods(allergene) {
    const foodsWithAllergene = this.foods.filter(food => food.allergenes.includes(allergene));
    return foodsWithAllergene;
  }

  // Sum the item with all the other items in an array,
  sumItems(item){
    this.day1Input.forEach(item => {
      const itemToNumber = parseInt(item);
      console.log(item + itemToNumber);
    });
  }

}
