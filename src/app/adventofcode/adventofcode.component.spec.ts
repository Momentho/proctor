import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdventofcodeComponent } from './adventofcode.component';

describe('AdventofcodeComponent', () => {
  let component: AdventofcodeComponent;
  let fixture: ComponentFixture<AdventofcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdventofcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdventofcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
