import { Component, OnInit } from '@angular/core';
import { PostService } from '../posts.service';
import {Subscription} from 'rxjs';

@Component({
  selector: '[post-list]',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  posts:any = [];

  public articles:any[] = [
    {
      id:'1',
      title:'Diam maecenas ultricies mi',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'2',
      title:'Auctor urna nunc',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'3',
      title:'Amet purus gravida',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'4',
      title:'Duis ultricies lacus',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'5',
      title:'Mi eget mauris',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'6',
      title:'Faucibus vitae aliquet',
      content: 'Amet aliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean euismod elementum',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
  ];


  private postsSub : Subscription;

  constructor(public postsService: PostService) {}

  ngOnInit() {
  }

}
