import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
// Reactive approach on teh form! See what is it
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { PostListComponent } from './post-list/post-list.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    PostListComponent
  ],
  exports: [
    PostListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class PostsModule{

}
