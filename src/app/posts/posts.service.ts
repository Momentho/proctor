import {Injectable} from '@angular/core'
import {Subject } from 'rxjs'
import {HttpClient, HttpClientModule} from '@angular/common/http'
import { Router } from '@angular/router'



const BACKEND_URL = 'https://jsonplaceholder.typicode.com/posts';


@Injectable({providedIn: 'root'})
export class PostService{

  private postData:any = [];
  //Using Subject of rxjs that allows you to observe for new posts
  private postsUpdated = new Subject <{posts: any[]}>();

// So to make a http request we need to inject the HttpClientModule imported in the app module
  constructor( private http: HttpClient , private router: Router){}

  getPosts() {
    this.http.get(BACKEND_URL).subscribe((res)=>{
      this.postData = res;
      this.postsUpdated.next({posts:[...this.postData]});
      console.log(this.postsUpdated);
    })
  }



  getPostsUpdatedListener(){
    return this.postsUpdated.asObservable();
  }
}
