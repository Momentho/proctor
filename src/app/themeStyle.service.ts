import {Injectable, Renderer2, RendererFactory2} from '@angular/core'


@Injectable({providedIn: 'root'})
export class ThemeStyleService{



  // Defining night/day status variable
  public themestatus: string;
  private renderer: Renderer2;


  constructor(rendererFactory: RendererFactory2){
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  _detectPrefersColorScheme() {
    // Detect if prefers-color-scheme is supported
     if (window.matchMedia('(prefers-color-scheme)').media !== 'not all') {
         // Set colorScheme to Dark if prefers-color-scheme is dark. Otherwise set to light.
         this.themestatus = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
    } else {
         // If the browser doesn't support prefers-color-scheme, set it as default to dark
        this.themestatus = 'dark';
    }
  }

  _setColorScheme(status) {
    this.themestatus = status;
    // Save prefers-color-scheme to localStorage
    localStorage.setItem('prefers-color', status);
  }

  _getColorScheme() {
    // Check if any prefers-color-scheme is stored in localStorage
    if (localStorage.getItem('prefers-color')) {
        // Save prefers-color-scheme from localStorage
        this.themestatus = localStorage.getItem('prefers-color');
    } else {
        // If no prefers-color-scheme is stored in localStorage, try to detect OS default prefers-color-scheme
        this._detectPrefersColorScheme();
    }
  }

  load() {
    this._getColorScheme();
    this.renderer.setAttribute(document.body, 'data-theme', this.themestatus);
  }

  update(scheme) {
    this._setColorScheme(scheme);
    // Add the new / current color-scheme class
    this.renderer.setAttribute(document.body, 'data-theme', scheme);
  }

  currentActive() {
      return this.themestatus;
  }


}
