// We have  always to put export in order to have the possibility to
//import it where I need it

export interface Post{
  id: string;
  title: string;
  subtitle: string;
  intro: string;
  content: string;
  imagePath: string;
  creator: string;
}
