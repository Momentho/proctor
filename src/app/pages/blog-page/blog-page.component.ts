import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.scss']
})
export class BlogPageComponent implements OnInit {

  public articles:any[] = [
    {
      id:'1',
      title:'Diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare aenean',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'2',
      title:'Nulla pharetra diam sit amet nisl suscipit adipiscing',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
    {
      id:'3',
      title:'Nulla pharetra diam sit amet nisl suscipit adipiscing',
      date: '20 November',
      category: 'Category',
      img: '/assets/images/placeholder.png',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
