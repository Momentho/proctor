# Procto Frontend Exercise

This is the entry project for the Procto Frontend Test.
- Angular is used just to manage the template and compile the scss of the components (no frontend framework like Material was used).

- Advent of Code exercise is inside `app/adventofcode` component (See section here on readme for more info).

**Please go trough the Project Structure section for more info.**

## Installation
This project is actually running on the following versions:

- Npm: 7.4.3
- Node version: v10.15.3

In order to install the project, clone it and run `npm install`, it should be ready to go! :)

## Gyp probelm
If the installatyion fail because of gyp (this will occure on MAC Catalina and up ). Please follow this page, it should be quick to fix:
- https://github.com/nodejs/node-gyp/blob/master/macOS_Catalina.md


## How to see the project (Angular development serve)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project structure.

**Scss strucure**
I decided to keep all the style in the src/assets/scss in order to create a similar structure to the one used in Patternlab and follow the logic of the components created in Adobe XD.

**Scss notes**
- In this project I used BEM to name my classes (except for the old day/night component). Css rules doesn't follow a particular order.

- I just considered two basic media query for the style.

**Components**
Components are managed in a different way (for time purpouse, not the same as the scss one as should be), I just created a basic page that holds some shared components:

- Posts folder for the articles list
- Shared folder that has the card and the text lock
- Pages where there is the blog page

**Day/night Theme**
This was not required for the design and it's not perfect, but I liked the idea to add this little touch with an old component that I created, hope you don't mind :) 


## Advent of Code
You can find teh code inside the `app/adventofcode` for the following exercise:

- https://adventofcode.com/2020/day/1

- https://adventofcode.com/2020/day/21

The **day 1** is solved, but unfortunately I wasn't able to find the final solution for **day 21**. I would like to share with you the code that I used in order to try to solve the puzzle (it's a bit dirty!!).

- http://localhost:4200/advent-of-code


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
